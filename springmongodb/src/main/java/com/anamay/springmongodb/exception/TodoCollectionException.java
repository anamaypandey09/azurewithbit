package com.anamay.springmongodb.exception;

public class TodoCollectionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1835855104851086574L;

	public TodoCollectionException(String message){
		super(message);
	}
	public static String notFoundException(String id) {
		return "Todo with " +id+" not found";
	}
	public static String TodoAlreadyExsist() {
		return "Todo with given name already exsists";
	}
}
