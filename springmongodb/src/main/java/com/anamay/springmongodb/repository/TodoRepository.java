package com.anamay.springmongodb.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.anamay.springmongodb.model.TodoDAO;

@Repository
public interface TodoRepository extends MongoRepository<TodoDAO,String> {
	
	@Query("{'todo':?0}")
	Optional<TodoDAO>findByTodo(String todo);
}
